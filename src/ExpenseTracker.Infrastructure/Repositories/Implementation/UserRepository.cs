using ExpenseTracker.Core.Entities;
using ExpenseTracker.Core.Repositories.Interface;

namespace ExpenseTracker.Infrastructure.Repositories.Implementation
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        
    }
}